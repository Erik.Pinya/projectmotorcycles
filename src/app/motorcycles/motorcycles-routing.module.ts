import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotorcyclesPage } from './motorcycles.page';

const routes: Routes = [
  {
    path: '',
    component: MotorcyclesPage
  },
  {
    path: 'add-motorcycle',
    loadChildren: () => import('./add-motorcycle/add-motorcycle.module').then(m => m.AddMotorcyclePageModule)
  },
  {
    path: ':motorcycleId',
    loadChildren: () => import('./motorcycle-detail/motorcycle-detail.module').then(m => m.MotorcycleDetailPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotorcyclesPageRoutingModule { }
