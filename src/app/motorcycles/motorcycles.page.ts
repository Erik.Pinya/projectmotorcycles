import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { Motorcycle, MotorcycleFilter } from './motorcycles.model';

@Component({
  selector: 'app-motorcycles',
  templateUrl: './motorcycles.page.html',
  styleUrls: ['./motorcycles.page.scss'],
})
export class MotorcyclesPage implements OnInit {
  motorcycles: Motorcycle[] = [];
  singleMotorcycle: Motorcycle;

  motorcyclesFilter: MotorcycleFilter[] = [
    {
      title: 'Ducati',
      image:
        'https://1000marcas.net/wp-content/uploads/2019/12/Ducati-logo.png',
    },

    {
      title: 'Yamaha',
      image:
        'https://images-na.ssl-images-amazon.com/images/I/51Tfykorz%2BL._AC_SY355_.jpg',
    },

    {
      title: 'Honda',
      image:
        'http://2.bp.blogspot.com/-w5AEVtVEl0o/UL8h3NlV5YI/AAAAAAAABPU/nenGJjai8Zk/s1600/Honda-Motorcycle-Logo.jpg',
    },
    
    {
      title: 'Todas',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/All-currency-symbol.svg/1024px-All-currency-symbol.svg.png',
    },
  ];

  constructor(private data: DataService, private router: Router) {}

  ngOnInit() {
    this.loadMotorcycles();
  }

  ionViewWillEnter() {
    this.loadMotorcycles();
  }

  loadMotorcycles() {
    (async () => {
      this.motorcycles = await this.data.getMotorcycles();
    })();
  }

  filterMotorcycles(brand: string) {
    if (brand === 'Todas') {
      this.loadMotorcycles();
    } else {
      (async () => {
        this.motorcycles = await this.data.getFilteredMotorcycle(
          brand.toLowerCase()
        );
      })();
    }
  }

  addNewMotorcycle() {
    this.router.navigate(['/motorcycles/add-motorcycle']);
  }
}
