export interface Motorcycle {
  id: number;
  marca?: string;
  modelo?: string;
  year?: string;
  foto?: string;
  precio?: string;
}

export interface MotorcycleFilter {
  title: string;
  image: string;
}
