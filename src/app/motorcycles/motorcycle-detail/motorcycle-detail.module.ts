import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MotorcycleDetailPageRoutingModule } from './motorcycle-detail-routing.module';

import { MotorcycleDetailPage } from './motorcycle-detail.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MotorcycleDetailPageRoutingModule,
    ComponentsModule
  ],
  declarations: [MotorcycleDetailPage]
})
export class MotorcycleDetailPageModule { }
