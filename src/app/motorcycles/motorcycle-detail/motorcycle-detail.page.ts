import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Motorcycle } from '../motorcycles.model';

@Component({
  selector: 'app-motorcycle-detail',
  templateUrl: './motorcycle-detail.page.html',
  styleUrls: ['./motorcycle-detail.page.scss'],
})
export class MotorcycleDetailPage implements OnInit {
  loadedMotorcycle: Motorcycle;

  constructor(
    private alertController: AlertController,
    private data: DataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadedMotorcycle = this.data.getSingleMotorcycle();

    if (!this.loadedMotorcycle || this.loadedMotorcycle === undefined) {
      this.router.navigate(['/motorcycles']);
    }

    console.log(this.loadedMotorcycle);
  }

  deleteMotorcycle() {
    this.presentAlertConfirm();

  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cuidado!',
      message: 'Estás seguro de que deseas eliminar esta moto?',
      buttons: [
        {
          text: 'CANCELAR',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancelando');
          }
        }, {
          text: 'VALE',
          handler: () => {
            this.data.deleteMotorcycle(this.loadedMotorcycle.id);
            this.router.navigate(['/motorcycles']);
          }
        }
      ]
    });

    await alert.present();
  }

}
