import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Motorcycle } from 'src/app/motorcycles/motorcycles.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-motorcycle-info',
  templateUrl: './motorcycle-info.component.html',
  styleUrls: ['./motorcycle-info.component.scss'],
})
export class MotorcycleInfoComponent implements OnInit {

  @Input() motorcycles: Motorcycle[] = [];

  constructor(private data: DataService, private router: Router) { }

  ngOnInit() { }

  goToDetail(motorcycle: Motorcycle) {

    this.data.setSingleMotorcycle(motorcycle)


    this.router.navigate([`/motorcycles/${motorcycle.id}`]);
  }

}
