import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotorcycleInfoComponent } from './motorcycle-info/motorcycle-info.component';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';



@NgModule({
  declarations: [MotorcycleInfoComponent, HeaderComponent],
  imports: [
    CommonModule, IonicModule
  ],
  exports: [MotorcycleInfoComponent, HeaderComponent]
})
export class ComponentsModule { }
